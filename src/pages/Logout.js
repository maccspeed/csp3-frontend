import { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

function Logout() {
    const { unsetUser, setUser } = useContext(UserContext);
    unsetUser();
    useEffect(() => {
        setUser({id: null});
        Swal.fire({
            title: "Done!",
            icon: "success",
            text: "You have successfully logged out!"
        })
    })

    return (
    <>
        <Redirect to="/login" />
    </>
    );
}

export default Logout;