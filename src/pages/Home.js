import { Container } from 'react-bootstrap';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

function Home() {
    const data = {
        title: "Zuitt",
        content: "Products for everyone, everywhere",
        destination: "/products",
        label: "Buy now!"
    }
    return (
        <Container className="container-fluid p-2 mt-5">
            <Banner data={data} />
            <Highlights />
        </Container>
    );
}

export default Home;