import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';

function Products() {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetch('https://fierce-peak-73133.herokuapp.com/products/')
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setProducts(data.map(product => {
                return (
                    <ProductCard key={product.id} productProp={product} />
                );
            }))
        })
    }, []);

    return (
        <div className="container p-5 mt-5">
            <h1 className="text-center pb-5">Products</h1>
            {products}
        </div>
    );
}

export default Products;