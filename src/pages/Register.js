import { useState, useEffect, useContext } from 'react';
import { Redirect, useHistory, Link } from 'react-router-dom';
import { Container, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

function Register() {
    const {user} = useContext(UserContext);
    const history = useHistory();

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState(false);

    function registerUser(e) {
        e.preventDefault();
        fetch('https://fierce-peak-73133.herokuapp.com/users/checkEmail', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            if (data === true) {

                Swal.fire({
                    title: 'Duplicate email found',
                    icon: 'error',
                    text: 'Please provide a different email.'   
                });
            }
            else {
                fetch('https://fierce-peak-73133.herokuapp.com/users/register', {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        email: email,
                        password: password
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    if (data === true) {
                        setEmail('');
                        setPassword('');
                        setPassword2('');
        
                        Swal.fire({
                            title: 'Success! You are now registered',
                            icon: 'success',
                            text: 'Thank you for your registration.'
                        })
        
                        history.push("/login");
                    }
                    else {
                        Swal.fire({
                            title: 'Registration Failed',
                            icon: 'error',
                            text: 'Please try again.'
                        });
                    }
                });
            }
        });
    }

    useEffect(() => {
        if ((email !== '' && password !== '' && password2 !== '') && (password === password2)) {
            setIsActive(true);
        }
        else {
            setIsActive(false);
        }
    }, [email, password, password2]);

    return (
    <Container className="pt-5">
        <h1 className="text-center py-5">Register</h1>
    {
        (user.id !== null) ?
        <Redirect to="/products" />
        :
        <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group controlId="userEmail" className="pb-4">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email} 
                    onChange={e => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password" className="pb-4">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password} 
                    onChange={e => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2" className="pb-4">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password"
                    value={password2} 
                    onChange={e => setPassword2(e.target.value)}
                    required
                />
            </Form.Group>

            { isActive ? 
                <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                </Button>
                : 
                <Button variant="secondary" type="submit" id="submitBtn" disabled>
                    Submit
                </Button>
            }
            <Form.Text className="text-muted">
                Already have an account? Click <Link to="/login">here</Link> to log in.
            </Form.Text>
        </Form>
    }</Container>
    );
}

export default Register;