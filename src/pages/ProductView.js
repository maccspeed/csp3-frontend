import { useState } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';

function ProductView() {
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);

    return (
    <Container className="mt-5">
        <Col>
            <Card>
                <Card.Body className="text-center">
                    <Card.Title>{name}</Card.Title>
                    <Card.Subtitle>Description</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Title>Price</Card.Title>
                    <Card.Subtitle>{price}</Card.Subtitle>
                </Card.Body>
            </Card>
        </Col>
    </Container>
    )
}

export default ProductView;