import { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router';
import { Form, Button, Container } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

function Login(props) {
    const {user, setUser} = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    function authenticate(e) {
        e.preventDefault();

        fetch('https://fierce-peak-73133.herokuapp.com/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            if (typeof data.access !== "undefined") {
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                    title: "Login Success!!",
                    icon: "success",
                    text: "You are now logged in."
                })
            }
            else {
                Swal.fire({
                    title: "Login failed",
                    icon: "error",
                    text: "Invalid user credentials. Please try again."
                })
            }
        })

        setEmail('');
        setPassword('');
    }

    const retrieveUserDetails = (token) => {
        fetch('https://fierce-peak-73133.herokuapp.com/users/details', {
            headers: {
                Authorization: `Bearer ${ token }`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
        })
    }

    useEffect(() => {
        if (email !== '' && password !== '') {
            setIsActive(true);
        }
        else {
            setIsActive(false);
        }
    }, [email, password]);

    return (
    <Container className="mt-5">
        <h1 className="text-center py-5">Login</h1>
    {
        (user.id !== null) ?
        <Redirect to="/products" />
        :
        <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group controlId="userEmail" className="pb-4">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password" className="pb-4">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>
            { isActive ? 
            <Button variant="primary" type="submit" id="submitBtn">
                Submit
            </Button>
            : 
            <Button variant="secondary" type="submit" id="submitBtn" disabled>
                Submit
            </Button>
            }

            <Form.Text className="text-muted">
                Don't have an account yet? Click <Link to="/register">here</Link> to register.
            </Form.Text>
        </Form>
    }</Container>
    );
}

export default Login;