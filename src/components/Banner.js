import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function Banner({data}) {
	console.log(data)
	const { title, content, destination, label } = data;
	
    return (
        <Row>
            <Col className="p-5">
                <h1>{title}</h1>
                <p>{content}</p>
                {/*<Link to={destination}>{label}</Link>*/}
                <Link to={destination}>
                    <Button variant="primary" to={destination}>{label}</Button>
                </Link>
            </Col>
        </Row>
    );
}

export default Banner;