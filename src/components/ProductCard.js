import PropTypes from 'prop-types'
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

function ProductCard({productProp}) {
	const { _id, name, description, price } = productProp;

    return (
        <Card className="mb-3">
            <Card.Body >
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PHP {price}</Card.Text>
                <Button variant="outline-info" as={Link} to={`/products/${_id}`}>Product Details</Button>
            </Card.Body>
        </Card>
    )
}

// Checks the validity of the PropTypes
ProductCard.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

export default ProductCard;