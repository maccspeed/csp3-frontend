import { useContext } from "react";
import { Nav, Navbar } from "react-bootstrap";
import { Link } from "react-router-dom";
import UserContext from '../UserContext';

function AppNavbar() {
    const { user } = useContext(UserContext);

    return (
    <Navbar bg="dark" variant="dark" collapseOnSelect expand="lg" fixed="top">
        <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
                <Nav.Link as={Link} to="/products">Products</Nav.Link>
            </Nav>
            {(user.id == null) ?
            <Nav>
                <Nav.Link as={Link} to="/register">Register</Nav.Link>
                <Nav.Link as={Link} to="/login">Login</Nav.Link>
            </Nav>
            :
            <Nav>
                <Nav.Link as={Link} to="/users/order">Orders</Nav.Link>
                <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
            </Nav>
            }
        </Navbar.Collapse>
    </Navbar>
    );
}

export default AppNavbar;