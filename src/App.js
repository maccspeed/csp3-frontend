import { useState, useEffect } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { UserProvider } from './UserContext';
import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home"
import Products from "./pages/Products"
import Register from "./pages/Register"
import Login from "./pages/Login";
import Logout from "./pages/Logout"

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })
  const unsetUser = () => {
    localStorage.clear();
  }
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <BrowserRouter>
        <AppNavbar />
        <Switch>
          <Route exact path="/"><Home /></Route>
          <Route path="/products"><Products /></Route>
          <Route path="/register"><Register /></Route>
          <Route path="/login"><Login /></Route>
          <Route path="/logout"><Logout /></Route>
        </Switch>
      </BrowserRouter>
    </UserProvider>
  );
}

export default App;
